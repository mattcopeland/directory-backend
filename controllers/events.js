var Event = require('mongoose').model('Event');
var moment = require('moment');

exports.getEvents = function (req, res) {
  Event.find({
    deleted: {
      $ne: true
    }
  }).exec(function (err, collection) {
    res.send(collection);
  });
};

exports.getEventsToday = function (req, res) {
  var today = moment(new Date())
    .startOf('day')
    .toISOString();
  var tomorrow = moment(new Date())
    .add(1, 'days')
    .startOf('day')
    .toISOString();
  Event.find({
    deleted: {
      $ne: true
    },
    start: {
      $lt: tomorrow,
      $gte: today
    }
  }).exec(function (
    err,
    collection
  ) {
    res.send(collection);
  });
};

exports.createEvent = function (req, res) {
  var eventData = req.body;
  eventData.owner = req.user._id;
  Event.create(eventData, function (err, event) {
    if (err) {
      res.status(400);
      return res.send({
        reason: err.toString()
      });
    }
    res.status(200);
    return res.end();
  });
};

exports.updateEvent = function (req, res) {
  var eventUpdates = req.body;
  if (
    req.user.roles.indexOf('admin') < 0 &&
    req.user._id !== eventUpdates.owner
  ) {
    res.status(403);
    return res.end();
  }

  Event.findByIdAndUpdate(
    eventUpdates.id,
    eventUpdates, {
      new: true
    },
    function (err, event) {
      if (err) {
        res.status(400);
        return res.send({
          reason: err.toString()
        });
      }
      res.status(200);
      return res.end();
    }
  );
};

exports.deleteEvent = function (req, res) {
  var event = req.body;
  if (req.user.roles.indexOf('admin') < 0 && req.user._id !== event.owner) {
    res.status(403);
    return res.end();
  }

  Event.findByIdAndUpdate(event.id, {
    $set: {
      deleted: true
    }
  }, function (
    err,
    event
  ) {
    if (err) {
      res.status(400);
      return res.send({
        reason: err.toString()
      });
    }
    res.status(200);
    return res.end();
  });
};