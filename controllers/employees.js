var Employee = require('mongoose').model('Employee');
var FunFact = require('mongoose').model('FunFact');
var FeaturedEmployee = require('mongoose').model('FeaturedEmployee');
var _ = require('lodash');
var fs = require('fs');
var path = require('path');
var moment = require('moment');

/**
 * Get employees
 *
 * @param {*} req
 * @param {*} res
 */
exports.getEmployees = function (req, res) {
  Employee.aggregate([{
      $match: {
        deleted: {
          $ne: true
        }
      }
    },
    {
      $lookup: {
        from: 'offices',
        localField: 'officeCode',
        foreignField: 'code',
        as: 'office'
      }
    },
    {
      $lookup: {
        from: 'funfacts',
        localField: 'eid',
        foreignField: 'eid',
        as: 'funFact'
      }
    },
    {
      $addFields: {
        office: {
          $arrayElemAt: ['$office', 0]
        },
        funFact: {
          $arrayElemAt: ['$funFact', 0]
        }
      }
    }
  ]).exec(function (err, employees) {
    // Do some formatting fo the employee data
    _.forEach(employees, function (employee) {
      // Create a fullName property for each employee
      employee.fullName = employee.firstName + ' ' + employee.lastName;

      // Set a default image
      if (!employee.image) {
        employee.image = 'profile.png';
      }

      // Create a displayName property for each employee
      if (employee.nickname) {
        employee.displayName = employee.nickname + ' ' + employee.lastName;
      } else {
        employee.displayName = employee.firstName + ' ' + employee.lastName;
      }

      // Create a manager property for each employee
      var manager = _.find(employees, {
        eid: employee.mid
      });
      if (manager) {
        if (manager.nickname) {
          employee.manager = manager.nickname + ' ' + manager.lastName;
        } else {
          employee.manager = manager.firstName + ' ' + manager.lastName;
        }
      }

      if (employee.hireDate) {
        employee.yearsOfService = moment().diff(employee.hireDate, 'years');
      }

      // Flatten out the fun fact a bit
      if (employee.funFact) {
        employee.funFact = employee.funFact.funFact;
      }

      // Combine office city and state
      if (employee.office) {
        employee.officeCityState =
          employee.office.city + ', ' + employee.office.state;
      }

      // Create committee display names
      if (employee.committees) {
        var committees = [];
        _.forEach(employee.committees, function (committee) {
          if (committee === 'fun-games') {
            committees.push({
              name: 'Fun & Games',
              code: committee
            });
          } else if (committee === 'wellness') {
            committees.push({
              name: 'Wellness',
              code: committee
            });
          } else if (committee === 'beautification') {
            committees.push({
              name: 'Beautification',
              code: committee
            });
          } else if (committee === 'diversity') {
            committees.push({
              name: 'Diversity',
              code: committee
            });
          }
        });
        employee.committees = committees;
      }

      // Create guild display names
      if (employee.guilds) {
        var guilds = [];
        _.forEach(employee.guilds, function (guild) {
          if (guild === 'front-end') {
            guilds.push({
              name: 'Front-End',
              code: guild
            });
          } else if (guild === 'architecture') {
            guilds.push({
              name: 'Architecture',
              code: guild
            });
          } else if (guild === 'quality') {
            guilds.push({
              name: 'Quality',
              code: guild
            });
          }
        });
        employee.guilds = guilds;
      }
    });

    employees = _.sortBy(employees, ['lastName', 'firstName']);

    createFeaturedEmployee(employees);

    res.send(employees);
  });
};

exports.getFeaturedEmployee = function (req, res) {
  var today = moment().startOf('day');
  FeaturedEmployee.findOne({
    date: {
      $eq: today
    }
  }).exec(function (err, fe) {
    if (err) {
      console.log("didn't find one");
    } else {
      res.send(fe);
    }
  });
};

exports.getDeletedEmployees = function (req, res) {
  Employee.aggregate([{
      $match: {
        deleted: {
          $eq: true
        }
      }
    },
    {
      $lookup: {
        from: 'offices',
        localField: 'officeCode',
        foreignField: 'code',
        as: 'office'
      }
    },
    {
      $addFields: {
        office: {
          $arrayElemAt: ['$office', 0]
        }
      }
    }
  ]).exec(function (err, employees) {
    _.forEach(employees, function (employee) {
      // Create a fullName property for each employee
      employee.fullName = employee.firstName + ' ' + employee.lastName;

      // Create a displayName property for each employee
      if (employee.nickname) {
        employee.displayName = employee.nickname + ' ' + employee.lastName;
      } else {
        employee.displayName = employee.firstName + ' ' + employee.lastName;
      }
    });

    res.send(employees);
  });
};

exports.createEmployee = function (req, res) {
  var employeeData = req.body;
  // Only admins can add employees
  if (req.user.roles.indexOf('admin') < 0) {
    res.status(403);
    return res.send({
      message: 'Not Authroized'
    });
  }

  Employee.create(employeeData, function (err, employee) {
    if (err) {
      if (err.toString().indexOf('E11000') > -1) {
        err = new Error('Employee ID / email already in use');
      }
      res.status(400);
      return res.send({
        message: err.toString()
      });
    }

    res.send(employee);
  });
};

exports.assignDesk = function (req, res) {
  // Users can only update their own desk, unless they are an admin
  if (req.user.eid !== req.body.eid && req.user.roles.indexOf('admin') < 0) {
    res.status(403);
    return res.send({
      message: 'Not Authroized'
    });
  }

  Employee.findOneAndUpdate({
    eid: req.body.eid
  }, {
    officeCode: req.body.officeCode,
    floor: req.body.floor,
    seat: req.body.seat
  }).exec(function (err, employee) {
    if (err) {
      console.log('error updating seat');
    } else {
      res.send(employee);
    }
  });
};

exports.makeRemote = function (req, res) {
  // Users can only update their own desk, unless they are an admin
  if (req.user.eid !== req.body.eid && req.user.roles.indexOf('admin') < 0) {
    res.status(403);
    return res.send({
      message: 'Not Authroized'
    });
  }

  Employee.findOneAndUpdate({
    eid: req.body.eid
  }, {
    officeCode: 'oth',
    floor: null,
    seat: null
  }).exec(function (err, employee) {
    if (err) {
      console.log('error updating seat');
    } else {
      res.send(employee);
    }
  });
};

exports.updateEmployee = function (req, res) {
  // Users can only update their own data, unless they are an admin
  if (req.user._id !== req.body._id && req.user.roles.indexOf('admin') < 0) {
    res.status(403);
    return res.send({
      message: 'Not Authroized'
    });
  }

  Employee.findByIdAndUpdate(req.body._id, req.body, function (err, employee) {
    if (err) {
      res.status(400);
      return res.send({
        reason: err.toString()
      });
    }
    return res.status(200).send(employee);
  });
};

exports.deleteEmployee = function (req, res) {
  // Only admins can add employees
  if (req.user.roles.indexOf('admin') < 0) {
    res.status(403);
    return res.send({
      message: 'Not Authroized'
    });
  }

  Employee.findOneAndUpdate({
      _id: req.body._id
    }, {
      $set: {
        deleted: true
      }
    },
    function (err, employee) {
      if (err) {
        res.status(400);
        return res.send({
          reason: err.toString()
        });
      }
      return res.status(200).send({
        message: 'Employee has been deleted'
      });
    }
  );
};

exports.restoreEmployee = function (req, res) {
  // Only admins can add employees
  if (req.user.roles.indexOf('admin') < 0) {
    res.status(403);
    return res.send({
      message: 'Not Authroized'
    });
  }

  Employee.findOneAndUpdate({
      _id: req.body._id
    }, {
      $set: {
        deleted: false
      }
    },
    function (err, employee) {
      if (err) {
        res.status(400);
        return res.send({
          reason: err.toString()
        });
      }
      return res.status(200).send({
        message: 'Employee has been deleted'
      });
    }
  );
};

exports.updateEmployeeAvatar = function (req, res) {
  // Users can only update their own desk, unless they are an admin
  if (req.user.eid !== req.body.eid && req.user.roles.indexOf('admin') < 0) {
    res.status(403);
    return res.send({
      message: 'Not Authroized'
    });
  }
  var image = req.body.avatar;
  var eid = req.body.eid;
  var data = image.replace(/^data:image\/\w+;base64,/, '');
  var fileName = eid + '_' + Date.now() + '.png';
  var photoDir = path.join(
    path.normalize(__dirname + '/../'),
    './avatars/employees/',
    fileName
  );
  fs.writeFile(photoDir, data, {
    encoding: 'base64'
  }, function (err) {
    fs.chmod(photoDir, 0755, error => {
      console.log('Changed file permissions');
    });
    console.log('Photo Uploaded');
    if (err) {
      res.status(500);
      res.json({
        success: false
      });
    } else {
      Employee.findOneAndUpdate({
        eid: eid
      }, {
        image: fileName
      }).exec(function (err, employee) {
        if (err) {
          console.log('error updating photo');
        }
      });
      res.status(200);
      res.send({
        fileName: fileName
      });
    }
  });
};

exports.updateEmployeeFunFact = function (req, res) {
  // Users can only update their own data, unless they are an admin
  if (req.user.eid !== req.body.eid && req.user.roles.indexOf('admin') < 0) {
    res.status(403);
    return res.send({
      message: 'Not Authroized'
    });
  }

  var funFactData = {
    eid: req.body.eid,
    funFact: req.body.funFact
  };

  FunFact.findOneAndUpdate({
      eid: req.body.eid
    },
    funFactData, {
      upsert: true
    },
    function (err, funFact) {
      if (err) {
        res.status(400);
        return res.send({
          reason: err.toString()
        });
      }
      return res.status(200).send(funFact);
    }
  );
};

function createFeaturedEmployee(employees) {
  var today = moment().startOf('day');
  FeaturedEmployee.findOne({
    date: {
      $eq: today
    }
  }).exec(function (err, fe) {
    if (fe) {
      // Nothing to do
    } else {
      // Remove excluded employees from potential featured employees list
      employees = employees.filter(employee => {
        return !employee.exclude;
      });
      var featuredEmployeeIndex = Math.floor(
        Math.random() * Math.floor(employees.length)
      );
      var featuredEmployee = {
        eid: employees[featuredEmployeeIndex].eid,
        date: moment().startOf('day')
      };

      FeaturedEmployee.create(featuredEmployee, function (err, employee) {
        if (err) {
          console.log(err);
        }
      });
    }
  });
}