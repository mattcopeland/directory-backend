var User = require('mongoose').model('User');
var Employee = require('mongoose').model('Employee');
var jwt = require('jwt-simple');
var bcrypt = require('bcrypt-nodejs');
var emails = require('./emails');
var uuid = require('uuid');
var credentials = require('../config/credentials');

exports.checkAuthenticated = (req, res, next) => {
  if (!req.header('authorization')) {
    return res
      .status(401)
      .send({
        message: 'Unauthorized.  Missing Auth header'
      });
  }
  // The auth token looks like 'token #####...'
  var token = req.header('authorization').split(' ');
  if (token.length < 2 || token[1] === 'null') {
    return res
      .status(401)
      .send({
        message: 'Unauthorized.  Missing Auth token'
      });
  }
  var payload = jwt.decode(token[1], credentials.tokenSecret);
  if (!payload) {
    return res
      .status(401)
      .send({
        message: 'Unauthorized.  Invaild Auth header'
      });
  }
  req.user = {
    _id: payload.sub,
    eid: payload.eid,
    roles: payload.roles
  };
  next();
};

exports.login = async (req, res) => {
  var loginData = req.body;
  var user = await User.findOne({
    email: loginData.email.toLowerCase()
  });

  if (!user) {
    res.status(401).send({
      message: 'Email or password invalid'
    });
  } else {
    bcrypt.compare(loginData.password, user.password, (err, isMatch) => {
      if (!isMatch) {
        res.status(401).send({
          message: 'Email or password invalid'
        });
      } else if (!user.verified) {
        res
          .status(401)
          .send({
            message: 'Email address has not been verified',
            code: 1
          });
      } else {
        Employee.findOne({
          email: loginData.email.toLowerCase()
        }).exec(function (err, employee) {
          if (err) {
            res.status(400);
            return res.send({
              message: err.toString()
            });
          }
          // If this employee doesn't exist in the system don't create an account for them
          if (!employee) {
            res.status(400);
            return res.send({
              message: 'Employee does not exist'
            });
          }
          createSendToken(res, user, employee);
        });
      }
    });
  }
};

exports.register = (req, res) => {
  var userData = req.body;
  userData.verificationToken = uuid();

  Employee.findOne({
    email: userData.email.toLowerCase()
  }).exec(function (err, employee) {
    if (err) {
      res.status(400);
      return res.send({
        message: err.toString()
      });
    }
    // If this employee doesn't exist in the system don't create an account for them
    if (!employee) {
      res.status(400);
      return res.send({
        message: 'Employee does not exist'
      });
    }

    userData._id = employee._id;

    User.create(userData, function (err, newUser) {
      if (err) {
        if (err.toString().indexOf('E11000') > -1) {
          err = 'Email already in use';
        }
        res.status(400);
        return res.send({
          message: err.toString()
        });
      }
      emails.userVerification(newUser, employee, req.get('host'));
      // createSendToken(res, newUser, employee);
      res.status(200).send({
        message: 'User has been registered'
      });
    });
  });
};

exports.verifyUser = async (req, res) => {
  var user = await User.findOneAndUpdate({
    _id: req.query.userId,
    verificationToken: req.query.verificationToken
  }, {
    $set: {
      verified: true
    }
  });

  if (!user) {
    res.status(401).send({
      message: 'User not verified'
    });
  } else {
    res.status(200).send({
      message: 'User has been verified'
    });
  }
};

exports.generatePasswordResetEmail = async (req, res) => {
  var verificationToken = uuid();
  var user = await User.findOneAndUpdate({
    email: req.query.email.toLowerCase()
  }, {
    $set: {
      verificationToken: verificationToken
    }
  }, {
    new: true
  });
  if (user) {
    emails.passwordReset(user, verificationToken, req.get('host'));
  }
  res.status(201).send(user);
};

exports.passwordReset = (req, res) => {
  // create a new verification token so that each can only be used once
  var verificationToken = uuid();
  var userData = req.body;
  bcrypt.hash(userData.password, null, null, (err, hash) => {
    if (err) {
      return next(err);
    }
    userData.password = hash;
    User.findOneAndUpdate({
      _id: userData.id,
      verificationToken: userData.token
    }, {
      $set: {
        password: userData.password,
        verificationToken: verificationToken
      }
    }).exec(function (err, user) {
      if (err) {
        res.status(500).send({
          message: 'Error'
        });
      } else if (user) {
        Employee.findOne({
          email: user.email.toLowerCase()
        }).exec(function (err, employee) {
          if (err) {
            res.status(400);
            return res.send({
              message: err.toString()
            });
          }
          if (!employee) {
            res.status(400);
            return res.send({
              message: 'Employee does not exist'
            });
          }
          createSendToken(res, user, employee);
        });
      } else {
        res.status(500).send({
          message: 'Error'
        });
      }
    });
  });
};

function createSendToken(res, user, employee) {
  var payload = {
    sub: user._id,
    eid: employee.eid,
    email: user.email.toLowerCase(),
    roles: user.roles,
    firstName: employee.firstName,
    lastName: employee.lastName,
    fullName: (employee.nickname || employee.firstName) + ' ' + employee.lastName,
    image: employee.image || 'profile.png',
    verified: user.verified
  };
  var token = jwt.encode(payload, credentials.tokenSecret);
  res.status(200).send({
    token
  });
}