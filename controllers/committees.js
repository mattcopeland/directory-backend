var Committee = require('mongoose').model('Committee');

exports.getCommittees = function (req, res) {
  Committee.find({}).exec(function (err, collection) {
    res.send(collection);
  });
};