var nodemailer = require('nodemailer');
var credentials = require('../config/credentials');
var _ = require('lodash');

var logo = 'https://i.postimg.cc/bvTwv1QX/wtw-name-logo.png';
var fromEmail = '"WTW Directory" <liazondirectory@gmail.com>';

exports.userVerification = function (user, employee, host) {
  var email = user.email;
  // Send all emails to admin email in dev enironment
  if (process.env.NODE_ENV === 'development') {
    email = 'mattcopeland@gmail.com';
  }

  var content = 'Click the button below to verify your account.';

  var link =
    '<a style="text-decoration:none; color:#FFF;" href="http://' +
    host +
    '/pages/auth/mail-confirm;u=' +
    user._id +
    ';t=' +
    user.verificationToken +
    '">Confirm my email</a>';

  var msg =
    '<div> <table data-bgcolor="BodyBg" align="center" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#383434" class=""> <tbody> <tr> <td align="center" valign="middle"> <table class="main" align="center" width="450" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td style="font-size:70px; line-height:70px;" align="center" valign="top" height="70">&nbsp;</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> <table data-bgcolor="BodyBg" align="center" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#383434" class=""> <tbody> <tr> <td align="center" valign="middle"> <table class="main" align="center" width="450" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td style="border-radius: 25px 25px 0px 0px; background-color: rgb(112, 32, 130);" data-bgcolor="theme-bg" align="center" valign="top" bgcolor="#efa149"> <table class="two-left" align="center" width="380" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td style="font-size:35px; line-height:35px;" align="center" valign="top" height="35">&nbsp;</td> </tr> <tr> <td align="center" valign="top"> <table align="center" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td align="center" valign="middle"><img editable="true" mc:edit="bm15-01" src="' +
    logo +
    '" alt="" width="300" height="17"></td> </tr> </tbody> </table> </td> </tr> <tr> <td style="font-size:20px; line-height:20px;" align="center" valign="top" height="20">&nbsp;</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> <div class="parentOfBg"> <table data-bgcolor="BodyBg" align="center" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#383434" class="currentTable"> <tbody> <tr> <td align="center" valign="middle"> <table class="main" align="center" width="450" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td align="center" valign="top" bgcolor="#FFFFFF"> <table class="two-left" align="center" width="380" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td style="font-size:60px; line-height:60px;" align="center" valign="top" height="60">&nbsp;</td> </tr> <tr> <td align="center" valign="top"> <table align="center" width="100%" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td style="font-family: Arial, Helvetica, sans-serif; font-size: 18px; color: rgb(112, 32, 130); font-weight: normal; line-height: 36px;" data-color="theme-colour" mc:edit="bm15-03" align="center" valign="top" contenteditable="true" class="editable"> <multiline>Welcome</multiline> </td> </tr> <tr> <td style="font-family:Arial, Helvetica, sans-serif; font-size:30px; color:#4c4c4c; font-weight:normal;" mc:edit="bm15-04" align="center" valign="top"> <multiline>' +
    employee.firstName +
    ' ' +
    employee.lastName +
    '</multiline> </td> </tr> <tr> <td style="font-size:10px; line-height:10px;" align="center" valign="top" height="10">&nbsp;</td> </tr> <tr> <td style="font-family:\'Open Sans\', sans-serif, Verdana; font-size:15px; color:#4c4c4c; font-weight:normal; line-height:24px; padding:0px 35px;" mc:edit="bm15-05" align="center" valign="top"> <multiline>' +
    content +
    '</multiline> </td> </tr> <tr> <td align="center" valign="top" height="20">&nbsp;</td> </tr> <tr> <td align="center" valign="top"> <table align="center" width="205" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td style="font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: rgb(255, 255, 255); font-weight: bold; border-radius: 30px; background-color: rgb(112, 32, 130);" data-bgcolor="theme-bg" mc:edit="bm15-06" align="center" valign="middle" height="50" bgcolor="#efa149"> <multiline>' +
    link +
    '</multiline> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style="font-size:60px; line-height:60px;" align="center" valign="top" height="80">&nbsp;</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </div> <table data-bgcolor="BodyBg" align="center" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#383434" class=""> <tbody> <tr> <td align="center" valign="middle"> <table class="main" align="center" width="450" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td data-bgcolor="theme-bg" style="border-radius: 0px 0px 25px 25px; background-color: rgb(112, 32, 130);" align="center" valign="top" bgcolor="#efa149">&nbsp;</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> <table data-bgcolor="BodyBg" align="center" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#383434" class=""> <tbody> <tr> <td align="center" valign="middle"> <table class="main" align="center" width="450" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td align="center" valign="top"> <table class="two-left" align="center" width="380" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td style="font-size:35px; line-height:35px;" align="center" valign="top" height="35">&nbsp;</td> </tr> <tr> <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#FFF; font-weight:normal; line-height:28px;" mc:edit="bm15-07" align="center" valign="top"> <multiline>WTW / Liazon Employee Directory</multiline> </td> </tr> <tr> <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#FFF; font-weight:bold; line-height:28px;" mc:edit="bm15-08" align="center" valign="top"> <multiline>Copyright © 2018 The Maestro</multiline> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> <table data-bgcolor="BodyBg" align="center" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#383434" class=""> <tbody> <tr> <td align="center" valign="middle"> <table class="main" align="center" width="450" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td style="font-size:90px; line-height:90px;" align="center" valign="top" height="90">&nbsp;</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </div>';

  var mailOptions = {
    from: fromEmail, // sender address
    to: email, // list of receivers
    subject: 'Directory Verification', // Subject line
    text: 'Please verify your account at http://' +
      host +
      '/pages/auth/mail-confirm;u=' +
      user._id +
      ';t=' +
      user.verificationToken, // plaintext body
    html: msg // html body
  };

  sendMail(mailOptions);
};

exports.passwordReset = function (user, verificationToken, host) {
  var email = user.email;
  // Send all emails to admin email in dev enironment
  if (process.env.NODE_ENV === 'development') {
    email = 'mattcopeland@gmail.com';
  }

  var content = 'Click the button below to reset your password.';

  var link =
    '<a style="text-decoration:none; color:#FFF;" href="http://' +
    host +
    '/pages/auth/reset-password;u=' +
    user._id +
    ';t=' +
    verificationToken +
    '">Reset my password</a>';

  var msg =
    '<div> <table data-bgcolor="BodyBg" align="center" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#383434" class=""> <tbody> <tr> <td align="center" valign="middle"> <table class="main" align="center" width="450" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td style="font-size:70px; line-height:70px;" align="center" valign="top" height="70">&nbsp;</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> <table data-bgcolor="BodyBg" align="center" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#383434" class=""> <tbody> <tr> <td align="center" valign="middle"> <table class="main" align="center" width="450" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td style="border-radius: 25px 25px 0px 0px; background-color: rgb(112, 32, 130);" data-bgcolor="theme-bg" align="center" valign="top" bgcolor="#efa149"> <table class="two-left" align="center" width="380" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td style="font-size:35px; line-height:35px;" align="center" valign="top" height="35">&nbsp;</td> </tr> <tr> <td align="center" valign="top"> <table align="center" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td align="center" valign="middle"><img editable="true" mc:edit="bm15-01" src="' +
    logo +
    '" alt="" width="300" height="17"></td> </tr> </tbody> </table> </td> </tr> <tr> <td style="font-size:20px; line-height:20px;" align="center" valign="top" height="20">&nbsp;</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> <div class="parentOfBg"> <table data-bgcolor="BodyBg" align="center" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#383434" class="currentTable"> <tbody> <tr> <td align="center" valign="middle"> <table class="main" align="center" width="450" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td align="center" valign="top" bgcolor="#FFFFFF"> <table class="two-left" align="center" width="380" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td style="font-size:60px; line-height:60px;" align="center" valign="top" height="60">&nbsp;</td> </tr> <tr> <td align="center" valign="top"> <table align="center" width="100%" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td style="font-family: Arial, Helvetica, sans-serif; font-size: 18px; color: rgb(112, 32, 130); font-weight: normal; line-height: 36px;" data-color="theme-colour" mc:edit="bm15-03" align="center" valign="top" contenteditable="true" class="editable"> <multiline>WTW Directory</multiline> </td> </tr> <tr> <td style="font-family:Arial, Helvetica, sans-serif; font-size:30px; color:#4c4c4c; font-weight:normal;" mc:edit="bm15-04" align="center" valign="top"> <multiline>Password Reset</multiline> </td> </tr> <tr> <td style="font-size:10px; line-height:10px;" align="center" valign="top" height="10">&nbsp;</td> </tr> <tr> <td style="font-family:\'Open Sans\', sans-serif, Verdana; font-size:15px; color:#4c4c4c; font-weight:normal; line-height:24px; padding:0px 35px;" mc:edit="bm15-05" align="center" valign="top"> <multiline>' +
    content +
    '</multiline> </td> </tr> <tr> <td align="center" valign="top" height="20">&nbsp;</td> </tr> <tr> <td align="center" valign="top"> <table align="center" width="205" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td style="font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: rgb(255, 255, 255); font-weight: bold; border-radius: 30px; background-color: rgb(112, 32, 130);" data-bgcolor="theme-bg" mc:edit="bm15-06" align="center" valign="middle" height="50" bgcolor="#efa149"> <multiline>' +
    link +
    '</multiline> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style="font-size:60px; line-height:60px;" align="center" valign="top" height="80">&nbsp;</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </div> <table data-bgcolor="BodyBg" align="center" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#383434" class=""> <tbody> <tr> <td align="center" valign="middle"> <table class="main" align="center" width="450" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td data-bgcolor="theme-bg" style="border-radius: 0px 0px 25px 25px; background-color: rgb(112, 32, 130);" align="center" valign="top" bgcolor="#efa149">&nbsp;</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> <table data-bgcolor="BodyBg" align="center" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#383434" class=""> <tbody> <tr> <td align="center" valign="middle"> <table class="main" align="center" width="450" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td align="center" valign="top"> <table class="two-left" align="center" width="380" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td style="font-size:35px; line-height:35px;" align="center" valign="top" height="35">&nbsp;</td> </tr> <tr> <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#FFF; font-weight:normal; line-height:28px;" mc:edit="bm15-07" align="center" valign="top"> <multiline>WTW / Liazon Employee Directory</multiline> </td> </tr> <tr> <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#FFF; font-weight:bold; line-height:28px;" mc:edit="bm15-08" align="center" valign="top"> <multiline>Copyright © 2018 The Maestro</multiline> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> <table data-bgcolor="BodyBg" align="center" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#383434" class=""> <tbody> <tr> <td align="center" valign="middle"> <table class="main" align="center" width="450" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td style="font-size:90px; line-height:90px;" align="center" valign="top" height="90">&nbsp;</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </div>';

  var mailOptions = {
    from: fromEmail, // sender address
    to: email, // list of receivers
    subject: 'Directory Password Reset', // Subject line
    text: 'Please verify your account at http://' +
      host +
      '/pages/auth/password-reset;u=' +
      user._id +
      ';t=' +
      user.verificationToken, // plaintext body
    html: msg // html body
  };

  sendMail(mailOptions);
};

exports.submitSuggestion = function (suggestionData) {
  var email = 'buffalo.facilities.services@willistowerswatson.com';
  var bcc = 'matt.copeland@liazon.com';
  // Send all emails to admin email in dev enironment
  if (process.env.NODE_ENV === 'development') {
    email = 'mattcopeland@gmail.com, matt.copeland@liazon.com';
  }

  var msg =
    '<html><head></head><body><div> <table data-bgcolor="BodyBg" align="center" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#383434" class=""> <tbody> <tr> <td align="center" valign="middle"> <table class="main" align="center" width="450" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td style="font-size:70px; line-height:70px;" align="center" valign="top" height="70">&nbsp;</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> <table data-bgcolor="BodyBg" align="center" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#383434" class=""> <tbody> <tr> <td align="center" valign="middle"> <table class="main" align="center" width="450" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td style="border-radius: 25px 25px 0px 0px; background-color: rgb(112, 32, 130);" data-bgcolor="theme-bg" align="center" valign="top" bgcolor="#efa149"> <table class="two-left" align="center" width="380" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td style="font-size:35px; line-height:35px;" align="center" valign="top" height="35">&nbsp;</td> </tr> <tr> <td align="center" valign="top"> <table align="center" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td align="center" valign="middle"><img editable="true" mc:edit="bm15-01" src="' +
    logo +
    '" alt="" width="300" height="17"></td> </tr> </tbody> </table> </td> </tr> <tr> <td style="font-size:20px; line-height:20px;" align="center" valign="top" height="20">&nbsp;</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> <div class="parentOfBg"> <table data-bgcolor="BodyBg" align="center" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#383434" class="currentTable"> <tbody> <tr> <td align="center" valign="middle"> <table class="main" align="center" width="450" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td align="center" valign="top" bgcolor="#FFFFFF"> <table class="two-left" align="center" width="380" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td style="font-size:60px; line-height:60px;" align="center" valign="top" height="60">&nbsp;</td> </tr> <tr> <td align="center" valign="top"> <table align="center" width="100%" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td style="font-family: Arial, Helvetica, sans-serif; font-size: 18px; color: rgb(112, 32, 130); font-weight: normal; line-height: 36px;" data-color="theme-colour" mc:edit="bm15-03" align="center" valign="top" contenteditable="true" class="editable"> <multiline>Suggestion from...</multiline> </td> </tr> <tr> <td style="font-family:Arial, Helvetica, sans-serif; font-size:30px; color:#4c4c4c; font-weight:normal;" mc:edit="bm15-04" align="center" valign="top"> <multiline>' +
    suggestionData.submitterName +
    '</multiline> </td> </tr> <tr> <td style="font-size:10px; line-height:10px;" align="center" valign="top" height="10">&nbsp;</td> </tr> <tr> <td style="font-family:\'Open Sans\', sans-serif, Verdana; font-size:15px; color:#4c4c4c; font-weight:normal; line-height:24px; padding:0px 35px;" mc:edit="bm15-05" align="center" valign="top"> <multiline>' +
    suggestionData.suggestion +
    '</multiline> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style="font-size:60px; line-height:60px;" align="center" valign="top" height="80">&nbsp;</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </div> <table data-bgcolor="BodyBg" align="center" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#383434" class=""> <tbody> <tr> <td align="center" valign="middle"> <table class="main" align="center" width="450" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td data-bgcolor="theme-bg" style="border-radius: 0px 0px 25px 25px; background-color: rgb(112, 32, 130);" align="center" valign="top" bgcolor="#efa149">&nbsp;</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> <table data-bgcolor="BodyBg" align="center" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#383434" class=""> <tbody> <tr> <td align="center" valign="middle"> <table class="main" align="center" width="450" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td align="center" valign="top"> <table class="two-left" align="center" width="380" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td style="font-size:35px; line-height:35px;" align="center" valign="top" height="35">&nbsp;</td> </tr> <tr> <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#FFF; font-weight:normal; line-height:28px;" mc:edit="bm15-07" align="center" valign="top"> <multiline>WTW / Liazon Employee Directory</multiline> </td> </tr> <tr> <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#FFF; font-weight:bold; line-height:28px;" mc:edit="bm15-08" align="center" valign="top"> <multiline>Copyright © 2018 The Maestro</multiline> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> <table data-bgcolor="BodyBg" align="center" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#383434" class=""> <tbody> <tr> <td align="center" valign="middle"> <table class="main" align="center" width="450" cellspacing="0" cellpadding="0" border="0"> <tbody> <tr> <td style="font-size:90px; line-height:90px;" align="center" valign="top" height="90">&nbsp;</td> </tr> </tbody> </table> </td> </tr> </tbody> </table></div></body></html>';

  var mailOptions = {
    from: fromEmail, // sender address
    to: email, // list of receivers
    bcc: bcc,
    subject: 'Directory Suggestion', // Subject line
    text: 'Suggestion from...' +
      suggestionData.submitterName +
      'Suggestion:' +
      suggestionData.suggestion, // plaintext body
    html: msg // html body
  };

  sendMail(mailOptions);
};

function sendMail(mailOptions) {
  if (process.env.NODE_ENV === 'development') {
    //return null;
  }
  var transporter = nodemailer.createTransport(credentials.smtp);

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      return console.log(error);
    }
    console.log('Message sent: ' + info.response);
  });
}