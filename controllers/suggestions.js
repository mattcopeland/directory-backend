var Suggestion = require('mongoose').model('Suggestion');
var emails = require('./emails');

exports.createSuggestion = function(req, res) {
  var suggestionData = req.body;
  suggestionData.submittedDate = new Date();

  Suggestion.create(suggestionData, function(err, suggestion) {
    if (err) {
      res.status(400);
      return res.send({
        message: err.toString()
      });
    }

    emails.submitSuggestion(suggestionData);
    res.send(suggestion);
  });
};
