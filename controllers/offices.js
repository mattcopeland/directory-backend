var Office = require('mongoose').model('Office');

exports.getOffices = function(req, res) {
  Office.find({}).exec(function(err, collection) {
    res.send(collection);
  });
};

exports.getOffice = function(req, res) {
  Office.findOne({
    code: req.query.code
  }).exec(function(err, collection) {
    res.send(collection);
  });
};
