var User = require('mongoose').model('User');

exports.getUsers = async (req, res) => {
  try {
    var users = await User.find({}, '-password -__v');
    res.send(users);
  } catch (error) {
    console.log(error);
    res.sendStatus(500);
  }
};

exports.getProfile = async (req, res) => {
  try {
    var user = await User.findById(req.params.id, '-password -__v');
    res.send(user);
  } catch (error) {
    console.log(error);
    res.sendStatus(500);
  }
};
