var employees = require('../controllers/employees');
var desks = require('../controllers/desks');
var auth = require('../controllers/auth');
var events = require('../controllers/events');
var offices = require('../controllers/offices');
var artifacts = require('../controllers/artifacts');
var rooms = require('../controllers/rooms');
var printers = require('../controllers/printers');
var jobs = require('../controllers/jobs');
var committees = require('../controllers/committees');
var suggestions = require('../controllers/suggestions');

module.exports = function (app, config) {
  // APIs
  app.post('/api/auth/login', auth.login);
  app.post('/api/auth/register', auth.register);
  app.get('/api/auth/verifyUser', auth.verifyUser);
  app.get(
    '/api/auth/generatePasswordResetEmail',
    auth.generatePasswordResetEmail
  );
  app.post('/api/auth/passwordReset', auth.passwordReset);

  // Employees
  app.get('/api/employees', employees.getEmployees);
  app.get('/api/employees/deleted', employees.getDeletedEmployees);
  app.get('/api/employees/featured', employees.getFeaturedEmployee);
  app.put(
    '/api/employee/update',
    auth.checkAuthenticated,
    employees.updateEmployee
  );
  app.post(
    '/api/employee/create',
    auth.checkAuthenticated,
    employees.createEmployee
  );
  app.put(
    '/api/employee/delete',
    auth.checkAuthenticated,
    employees.deleteEmployee
  );
  app.put(
    '/api/employee/restore',
    auth.checkAuthenticated,
    employees.restoreEmployee
  );
  app.put(
    '/api/employee/avatar',
    auth.checkAuthenticated,
    employees.updateEmployeeAvatar
  );
  app.post(
    '/api/employee/funFact',
    auth.checkAuthenticated,
    employees.updateEmployeeFunFact
  );
  app.put('/api/employee/desk', auth.checkAuthenticated, employees.assignDesk);
  app.put(
    '/api/employee/remote',
    auth.checkAuthenticated,
    employees.makeRemote
  );

  // Desks
  app.get('/api/desks', desks.getDesks);

  // Calendar
  app.get('/api/calendar/events', events.getEvents);
  app.get('/api/calendar/events/today', events.getEventsToday);
  app.post(
    '/api/calendar/events/create',
    auth.checkAuthenticated,
    events.createEvent
  );
  app.put(
    '/api/calendar/events/update',
    auth.checkAuthenticated,
    events.updateEvent
  );
  app.put(
    '/api/calendar/events/delete',
    auth.checkAuthenticated,
    events.deleteEvent
  );

  app.get('/api/desks', desks.getDesks);
  app.get('/api/offices', offices.getOffices);
  app.get('/api/office', offices.getOffice);
  app.get('/api/artifacts', artifacts.getArtifacts);
  app.get('/api/rooms', rooms.getRooms);
  app.get('/api/printers', printers.getPrinters);
  app.get('/api/jobs', jobs.getJobs);
  app.get('/api/committees', committees.getCommittees);
  app.post('/api/suggestion', suggestions.createSuggestion);

  // Serve up the index.html file and let angular handle the routing
  app.get('*', (req, res) => {
    res.sendFile(config.rootPath + '/build/public/dist/index.html');
  });
};