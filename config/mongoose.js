var mongoose = require('mongoose');
var userModel = require('../models/User');
var employeeModel = require('../models/Employee');
var officeModel = require('../models/Office');
var eventModel = require('../models/Event');
var deskModel = require('../models/Desk');
var artifactModel = require('../models/Artifact');
var jobModel = require('../models/Job');
var printerModel = require('../models/Printer');
var roomModel = require('../models/Room');
var funFactModel = require('../models/FunFact');
var featuredEmployeeModel = require('../models/FeaturedEmployee');
var suggestionModel = require('../models/Suggestion');
var committeeModel = require('../models/Committee');

module.exports = function (config) {
  mongoose.Promise = global.Promise;
  mongoose.connect(
    config.db, {
      useNewUrlParser: true
    }
  );
  var db = mongoose.connection;
  db.on('error', console.error.bind(console, 'connection error…'));
  db.once('open', function callback() {
    console.log('directory db opened');
  });
};

employeeModel.createDefaultEmployees();
officeModel.createDefaultOffices();
eventModel.createDefaultEvents();
deskModel.createDefaultDesks();