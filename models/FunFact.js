var mongoose = require('mongoose');

var funFactSchema = mongoose.Schema({
  eid: {
    type: Number,
    required: '{PATH} is required!'
  },
  funFact: {
    type: String,
    required: '{PATH} is required!'
  }
});

var FunFact = mongoose.model('FunFact', funFactSchema);
