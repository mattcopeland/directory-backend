var mongoose = require('mongoose');

var jobSchema = mongoose.Schema({
  department: {
    type: String,
    required: '{PATH} is required!'
  },
  jobTitle: {
    type: String,
    required: '{PATH} is required!'
  }
});

var Job = mongoose.model('Job', jobSchema);
