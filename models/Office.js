var mongoose = require('mongoose');

var officeSchema = mongoose.Schema({
  code: {
    type: String,
    required: '{PATH} is required!'
  },
  name: {
    type: String,
    required: '{PATH} is required!'
  },
  city: {
    type: String,
    required: '{PATH} is required!'
  },
  state: {
    type: String,
    required: '{PATH} is required!'
  },
  country: {
    type: String,
    required: '{PATH} is required!',
    default: 'employee'
  },
  address1: {
    type: String,
    required: '{PATH} is required!'
  },
  address2: {
    type: String
  },
  postalCode: {
    type: String,
    required: '{PATH} is required!'
  },
  building: {
    type: String
  },
  floors: {
    type: [Number]
  }
});

var Office = mongoose.model('Office', officeSchema);

function createDefaultOffices() {
  Office.find({}).exec(function(err, collection) {
    if (collection.length === 0) {
      Office.create({
        code: 'lzbuf',
        name: 'Liazon',
        city: 'Buffalo',
        state: 'NY',
        country: 'United States',
        address1: '199 Scott St',
        address2: 'Suite 800',
        postalCode: '14204',
        building: 'The Fairmont Creamery Building',
        floors: [6, 7, 8]
      });
    }
  });
}

exports.createDefaultOffices = createDefaultOffices;
