var mongoose = require('mongoose');

var committeeSchema = mongoose.Schema({
  code: {
    type: String,
    required: '{PATH} is required!',
    unique: true
  },
  name: {
    type: String,
    required: '{PATH} is required!'
  },
  description: {
    type: String,
    required: '{PATH} is required!'
  },
  eids: {
    type: [Number]
  }
});

var Committee = mongoose.model('Committee', committeeSchema);