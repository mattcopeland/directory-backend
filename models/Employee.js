var mongoose = require('mongoose');

var employeeSchema = mongoose.Schema({
  eid: {
    type: Number,
    required: '{PATH} is required!',
    unique: true
  },
  firstName: {
    type: String,
    required: '{PATH} is required!'
  },
  lastName: {
    type: String,
    required: '{PATH} is required!'
  },
  nickname: {
    type: String
  },
  birthdate: {
    type: Date
  },
  hireDate: {
    type: Date
  },
  email: {
    type: String,
    unique: true
  },
  phone: {
    type: Number
  },
  ext: {
    type: Number
  },
  department: {
    type: String,
    required: '{PATH} is required!'
  },
  jobTitle: {
    type: String,
    required: '{PATH} is required!'
  },
  jobClassification: {
    type: String
  },
  team: {
    type: String
  },
  officeCode: {
    type: String
  },
  floor: {
    type: Number
  },
  seat: {
    type: String
  },
  image: {
    type: String
  },
  exclude: {
    type: Boolean,
    default: false
  },
  mid: {
    type: Number
  },
  deleted: {
    type: Boolean,
    default: false
  }
});

var Employee = mongoose.model('Employee', employeeSchema);

function createDefaultEmployees() {
  Employee.find({}).exec(function(err, collection) {
    if (collection.length === 0) {
      Employee.create({
        eid: 1,
        firstName: 'Matthew',
        lastName: 'Copeland',
        nickname: 'Matt',
        email: 'mattcopeland@gmail.com',
        phone: '2121234567',
        ext: '123',
        department: 'Software Engineering',
        jobTitle: 'Software Engineer',
        team: 'Prestige Worldwide',
        officeCode: 'lzbuf',
        floor: 8,
        seat: '083006',
        image: '1.jpg',
        exclude: false,
        mid: 2
      });
    }
  });
}

exports.createDefaultEmployees = createDefaultEmployees;
