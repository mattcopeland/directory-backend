var mongoose = require('mongoose');

var eventSchema = mongoose.Schema({
  title: {
    type: String,
    required: '{PATH} is required!'
  },
  start: {
    type: Date,
    required: '{PATH} is required!'
  },
  end: {
    type: Date
  },
  allDay: {
    type: Boolean
  },
  color: {
    primary: String,
    secondary: String
  },
  resizable: {
    beforeStart: Boolean,
    afterEnd: Boolean
  },
  draggable: Boolean,
  meta: {
    location: String,
    notes: String
  },
  owner: {
    type: String
  },
  deleted: {
    type: Boolean,
    default: false
  }
});

var Event = mongoose.model('Event', eventSchema);

function createDefaultEvents() {
  Event.find({}).exec(function(err, collection) {
    if (collection.length === 0) {
      Event.create({
        start: new Date(),
        end: new Date(),
        startTime: '10:00 am',
        endTime: '12:00 pm',
        title: 'A 3 day event',
        allDay: false,
        color: {
          primary: '#ad2121',
          secondary: '#FAE3E3'
        },
        resizable: {
          beforeStart: true,
          afterEnd: true
        },
        draggable: true,
        meta: {
          location: 'Los Angeles',
          notes:
            'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
        },
        deleted: false
      });
    }
  });
}

exports.createDefaultEvents = createDefaultEvents;
