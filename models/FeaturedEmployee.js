var mongoose = require('mongoose');

var featuredEmployeeSchema = mongoose.Schema({
  eid: {
    type: Number,
    required: '{PATH} is required!'
  },
  date: {
    type: Date,
    required: '{PATH} is required'
  }
});

var FeaturedEmployee = mongoose.model(
  'FeaturedEmployee',
  featuredEmployeeSchema
);
