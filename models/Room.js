var mongoose = require('mongoose');

var roomSchema = mongoose.Schema({
  officecode: {
    type: String,
    required: '{PATH} is required!'
  },
  floor: {
    type: Number,
    required: '{PATH} is required!'
  },
  location: {
    type: String
  },
  name: {
    type: String,
    required: '{PATH} is required!'
  },
  classification: {
    type: 'String'
  },
  type: {
    type: String,
    default: 'meeting'
  },
  phone: {
    type: Number
  },
  ext: {
    type: Number
  },
  capacity: {
    type: Number
  },
  xpos: {
    type: Number,
    required: '{PATH} is required!'
  },
  ypos: {
    type: Number,
    required: '{PATH} is required!'
  },
  height: {
    type: Number,
    required: '{PATH} is required!'
  },
  width: {
    type: Number,
    required: '{PATH} is required!'
  }
});

var Room = mongoose.model('Room', roomSchema);

function createDefaultRooms() {
  Room.find({}).exec(function(err, collection) {
    if (collection.length === 0) {
      Room.create({
        officeCode: 'lzbuf',
        floor: 7,
        location: '1234',
        name: 'Break Room',
        type: 'meeting',
        phone: '5551212',
        ext: '123',
        capacity: '20',
        xpos: 10,
        ypos: 10,
        height: 100,
        width: 100
      });
    }
  });
}

exports.createDefaultRooms = createDefaultRooms;
