var mongoose = require('mongoose');

var suggestionSchema = mongoose.Schema({
  eid: {
    type: Number
  },
  suggestion: {
    type: String,
    required: '{PATH} is required!'
  },
  submitterName: {
    type: String
  },
  submittedDate: {
    type: Date,
    required: '{PATH} is required!'
  }
});

var Suggestion = mongoose.model('Suggestion', suggestionSchema);
