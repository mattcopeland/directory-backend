var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var userSchema = mongoose.Schema({
  email: {
    type: String,
    required: '{PATH} is required!',
    unique: true
  },
  password: {
    type: String,
    required: '{PATH} is required!'
  },
  roles: {
    type: Array,
    default: ['user']
  },
  verified: {
    type: Boolean,
    default: false
  },
  verificationToken: {
    type: String,
    select: false
  },
  created: {
    type: Date,
    required: true,
    default: Date.now
  }
});

userSchema.pre('save', function(next) {
  var user = this;

  if (!user || !user.isModified('password')) {
    return next;
  }

  bcrypt.hash(user.password, null, null, (err, hash) => {
    if (err) {
      return next(err);
    }
    user.password = hash;
    next();
  });
});

module.exports = mongoose.model('User', userSchema);
