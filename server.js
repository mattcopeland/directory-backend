var express = require('express');
var bodyParser = require('body-parser');
// var mongoose = require('mongoose');
var app = express();
var path = require('path');
var env = (process.env.NODE_ENV = process.env.NODE_ENV || 'development');
var config = require('./config/config')[env];

app.use(bodyParser.json());
app.use('/', express.static(path.join(__dirname, 'build/public/dist')));
app.use('/avatars', express.static(path.join(__dirname, 'avatars')));

require('./config/mongoose')(config);
require('./config/routes')(app, config);

app.listen(config.port);
console.log('Listening on port ' + config.port + '...');